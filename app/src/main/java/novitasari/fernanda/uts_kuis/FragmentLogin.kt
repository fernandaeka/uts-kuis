package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class FragmentLogin : AppCompatActivity() {
    private lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_login)
        button = findViewById(R.id.btnLogin)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal1::class.java))
        }
    }
}

