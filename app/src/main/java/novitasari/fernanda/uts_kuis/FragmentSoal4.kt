package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.frag_soal_1.*
import kotlinx.android.synthetic.main.frag_soal_1.txSoal1
import kotlinx.android.synthetic.main.frag_soal_4.*
import kotlinx.android.synthetic.main.frag_soal_5.*

class FragmentSoal4 : AppCompatActivity() , View.OnClickListener {

    private lateinit var button: Button
    var b: String = ""
    var resum: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_soal_4)
        button = findViewById(R.id.next4)
        button.setOnClickListener {

            startActivity(Intent(this, FragmentSoal5::class.java))
        }
        button = findViewById(R.id.back4)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal3::class.java))
        }
        registerForContextMenu(txSoal1)
        rbSoal4A.setOnClickListener(this)
    }
    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        when(v?.id){
            R.id.rbSoal4A-> {
                var mnuInflater = getMenuInflater()
                mnuInflater.inflate(R.menu.menu_popup4, menu)
            }
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this, v)
        popMenu.menuInflater.inflate(R.menu.menu_popup4, popMenu.menu)
        popMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.Item4A -> {
                    Toast.makeText(baseContext, "A. Everest", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item4B -> {
                    Toast.makeText(baseContext, "B. K2", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item4C -> {
                    Toast.makeText(baseContext, "C. Kangchenjunga", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item4D -> {
                    Toast.makeText(baseContext, "D. Jaya Wijaya", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }
        popMenu.show()

    }
}







