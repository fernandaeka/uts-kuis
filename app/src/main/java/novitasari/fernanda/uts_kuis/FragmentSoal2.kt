package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.frag_soal_1.*
import kotlinx.android.synthetic.main.frag_soal_1.txSoal1
import kotlinx.android.synthetic.main.frag_soal_2.*

class FragmentSoal2 : AppCompatActivity() , View.OnClickListener {

    private lateinit var button: Button
    var b: String = ""
    var resum: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_soal_2)
        button = findViewById(R.id.next2)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal3::class.java))
        }
        button = findViewById(R.id.back2)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal1::class.java))
        }
        registerForContextMenu(txSoal1)
        rbSoal2A.setOnClickListener(this)
    }
    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        when(v?.id){
            R.id.rbSoal2A-> {
                var mnuInflater = getMenuInflater()
                mnuInflater.inflate(R.menu.menu_popup2, menu)
            }
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this, v)
        popMenu.menuInflater.inflate(R.menu.menu_popup2, popMenu.menu)
        popMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.Item2A -> {
                    Toast.makeText(baseContext, "A. Magellan", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item2B -> {
                    Toast.makeText(baseContext, "B. Centaurus", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item2C -> {
                    Toast.makeText(baseContext, "C. Andromeda", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item2D -> {
                    Toast.makeText(baseContext, "D. Ursa Mayor", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }
        popMenu.show()

    }
}



