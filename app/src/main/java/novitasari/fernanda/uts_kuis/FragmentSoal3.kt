package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.frag_soal_1.*
import kotlinx.android.synthetic.main.frag_soal_1.txSoal1
import kotlinx.android.synthetic.main.frag_soal_3.*

class FragmentSoal3 : AppCompatActivity() , View.OnClickListener {

    private lateinit var button: Button
    var b: String = ""
    var resum: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_soal_3)
        button = findViewById(R.id.next3)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal4::class.java))
        }
        button = findViewById(R.id.back3)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal2::class.java))
        }
        registerForContextMenu(txSoal1)
        rbSoal3A.setOnClickListener(this)
    }
    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        when(v?.id){
            R.id.rbSoal3A-> {
                var mnuInflater = getMenuInflater()
                mnuInflater.inflate(R.menu.menu_popup3, menu)
            }
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this, v)
        popMenu.menuInflater.inflate(R.menu.menu_popup3, popMenu.menu)
        popMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.Item3A -> {
                    Toast.makeText(baseContext, "A. Lady Bug", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item3B -> {
                    Toast.makeText(baseContext, "B. Goliath", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item3C -> {
                    Toast.makeText(baseContext, "C. Firefly", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item3D -> {
                    Toast.makeText(baseContext, "D. Rhinoceros", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }
        popMenu.show()

    }
}



