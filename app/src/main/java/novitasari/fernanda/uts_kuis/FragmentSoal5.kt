package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.frag_soal_1.*
import kotlinx.android.synthetic.main.frag_soal_1.txSoal1
import kotlinx.android.synthetic.main.frag_soal_5.*

class FragmentSoal5 : AppCompatActivity() , View.OnClickListener {

    private lateinit var button: Button
    var b: String = ""
    var resum: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_soal_5)
        button = findViewById(R.id.next5)
        button.setOnClickListener {
            startActivity(Intent(this, SkorActivity::class.java))
        }
        button = findViewById(R.id.back5)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal4::class.java))
        }
        registerForContextMenu(txSoal1)
        rbSoal5A.setOnClickListener(this)
    }
    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        when(v?.id){
            R.id.rbSoal5A-> {
                var mnuInflater = getMenuInflater()
                mnuInflater.inflate(R.menu.menu_popup5, menu)
            }
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this, v)
        popMenu.menuInflater.inflate(R.menu.menu_popup5, popMenu.menu)
        popMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.Item5A -> {
                    Toast.makeText(baseContext, "A. Blue", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item5B -> {
                    Toast.makeText(baseContext, "B. Balaenoptera", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item5C -> {
                    Toast.makeText(baseContext, "C. Orca", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.Item5D -> {
                    Toast.makeText(baseContext, "D. Humpback", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }
        popMenu.show()

    }
}







