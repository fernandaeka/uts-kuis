package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class SkorActivity : AppCompatActivity()  {
    private lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skor)
        button = findViewById(R.id.btnHome)
        button.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
