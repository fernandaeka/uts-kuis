package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.frag_soal_1.*

class FragmentSoal1 : AppCompatActivity() , View.OnClickListener {

    private lateinit var button: Button
    var b: String = ""
    var resum: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_soal_1)
        button = findViewById(R.id.next1)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentSoal2::class.java))
        }
        button = findViewById(R.id.back1)
        button.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
        registerForContextMenu(txSoal1)
        rbSoal1A.setOnClickListener(this)
    }
    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        when(v?.id){
            R.id.rbSoal1A-> {
                var mnuInflater = getMenuInflater()
                mnuInflater.inflate(R.menu.menu_popup, menu)
            }
        }
    }

    override fun onClick(v: View?) {
            var popMenu = PopupMenu(this, v)
            popMenu.menuInflater.inflate(R.menu.menu_popup, popMenu.menu)
            popMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.Item1A -> {
                        Toast.makeText(baseContext, "A. Colombus", Toast.LENGTH_SHORT).show()
                        true
                    }
                    R.id.Item1B -> {
                        Toast.makeText(baseContext, "B. Van Den Boch", Toast.LENGTH_SHORT).show()
                        true
                    }
                    R.id.Item1C -> {
                        Toast.makeText(baseContext, "C. James Cook", Toast.LENGTH_SHORT).show()
                        true
                    }
                    R.id.Item1D -> {
                        Toast.makeText(baseContext, "D. Marco Polo", Toast.LENGTH_SHORT).show()
                        true
                    }
                }
                false
            }
            popMenu.show()

        }
    }



