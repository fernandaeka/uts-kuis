package novitasari.fernanda.uts_kuis

import android.app.AlertDialog
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.item_jawaban.view.*

class FragmentJawaban : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_jawaban)
    }
    lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View

    fun showJawaban(){
        v.lvJawab.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showJawaban()
    }
}
