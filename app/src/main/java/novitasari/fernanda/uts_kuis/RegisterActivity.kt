package novitasari.fernanda.uts_kuis

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class RegisterActivity : AppCompatActivity() {
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        button = findViewById(R.id.button2)
        button.setOnClickListener {
            startActivity(Intent(this, FragmentLogin::class.java))
        }
    }
}
